var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
 
/* By default each Schema contains _id property as a unique field*/
 
var userSchema = new Schema({
nohp: Number,
nama: String,
alamat: String,
});
 
exports.userModel = mongoose.model('my_db', userSchema);