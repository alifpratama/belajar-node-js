var express = require("express");
var bodyParser = require("body-parser");
const mongoose = require("mongoose");
var routes = require("./routes.js");
var app = express();
var url = "mongodb://localhost:27017/my_db";

mongoose
    .connect(url, { useNewUrlParser: true }, function(err, db) {
  		if (err) throw err;
  		const dbo = db.db("my_db");
  		dbo.collection("my_db").find({}, { projection: {} }).toArray(function(err, result) {
    	if (err) throw err;
    	console.log(result);
    	db.close();
  		});
	})
    .then(() => console.log("mongoDB Connected"))
    .catch((err) => console.log(err));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

routes(app);

var server = app.listen(3000, function () {
    console.log("app running on port.", server.address().port);
});